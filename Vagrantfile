# -*- mode: ruby -*-

$nodes_qty = 3
$start_ip = 20
$os = "ce212"

boxes = {
    "ce212" => "ce-stable",
    "se16" => "se16"
}

# Parse arguments.
args = Hash[ ARGV.flat_map{|s| s.scan(/--?([^=\s]+)(?:=(\S+))?/)} ]
args.each do |arg, value|
    case arg
        when "n"
            $nodes_qty = Integer(value)
        when "os"
            $os = value
        when "help"
            puts "    --n=<кол-во> количество узлов кластера"
    end
end

$vm_prefix = "prostor-#{$os}"
$box = boxes[$os]

def vm_hostname(node)
    return "#{$vm_prefix}-#{node}"
end

def vm_ip(node)
    return $start_ip + node
end

Vagrant.configure("2") do |config|
  # Nodes.
  (1..$nodes_qty).each do |node_id|
    host = vm_hostname(node_id)
    ip = vm_ip(node_id)
    config.vm.define host, autostart: true do |node|
      node.vm.box = "lab50/#{$box}"
      node.vm.guest = :debian
      node.vm.hostname = vm_hostname(node_id)

      node.ssh.insert_key = false

      node.vm.network :private_network,
          :auto_config => false,
          :ip => "192.168.2.#{ip}",
          :libvirt__dhcp_enabled => false,
          :libvirt__guest_ipv6 => false,
          :libvirt__network_address => "192.168.2.0",
          :libvirt__network_name => "cluster-data"

      node.vm.network :private_network,
          :auto_config => true,
          :ip => "172.16.17.#{ip}",
          :libvirt__dhcp_enabled => false,
          :libvirt__guest_ipv6 => false,
          :libvirt__network_address => "172.16.16.0",
          :libvirt__netmask => "255.255.255.0",
          :libvirt__forward_mode => "none",
          :libvirt__network_name => "cluster"

      node.vm.synced_folder './', '/vagrant', disabled: true

      if node_id == $nodes_qty
        node.vm.provision :ansible do |ansible|
          # ansible.verbose = "v"
          ansible.limit = "all"
          ansible.playbook = "cluster-test.yml"
          ansible.groups = {
              "nodes" => ["#{$vm_prefix}-[1:#{$nodes_qty}]"],
          }
          ansible.host_vars = (1..$nodes_qty).map{
              |node| [vm_hostname(node), { "ip" => vm_ip(node), "os" => $os }]
          }.to_h
        end
      end
    end
  end

  config.vm.provider :libvirt do |libvirt|
#      libvirt.host = "mysuperserver"
#      libvirt.username = "user"
#      libvirt.uri="qemu+tcp://mysuperserver/system"
#      libvirt.graphics_ip = '0.0.0.0'
#      libvirt.connect_via_ssh = true
      libvirt.memory = 1024
      libvirt.nested = true
      libvirt.default_prefix = ""
  end

  config.ssh.keep_alive = false
end

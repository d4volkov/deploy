pref("network.negotiate-auth.trusted-uris", "http://");
pref("network.negotiate-auth.delegation-uris", "http://");

pref("network.protocol-handler.expose.vnc", false);
pref("network.protocol-handler.expose.rdp", false);
pref("network.protocol-handler.expose.spice", false);

user_pref("browser.startup.page", 1);
user_pref("browser.startup.homepage", "http://{{ common.public.host }}");
